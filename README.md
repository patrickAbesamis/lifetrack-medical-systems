# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is for the technical exam for Lifetrack Medical Solutions

### How do I get set up? ###

* Clone or fork the repository
* Get into the root directory of the project
* Run "php artisan serve" from the command line or terminal
* Check "localhost:8000/" to access the application

### To access the API ###

* Do the same setup done above
* On the address bar on Postman put "localhost:8000/api/forecast"
* Use GET request type
* Add the following parameters along with a value
1. months
2. numberOfStudy
3. growthPerMonth
