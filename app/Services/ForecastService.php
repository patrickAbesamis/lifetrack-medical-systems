<?php
namespace App\Services;

class ForecastService
{
    protected $ramCost = .00553;

    public function getForecast($request) {
        $numberOfMonths = $request->input('months');
        $currentMonth = date('F Y');
        $arrayOfMonths = array();
        $return = array();
        for($i=0; $i < $numberOfMonths; $i++) {
            $month = date('F Y', strtotime("+" . $i . " months", strtotime($currentMonth)));
            $arrayOfMonths[] = $month;
            $numberOfStudy = $request->input('numberOfStudy');
            $growthPerMonth = $request->input('growthPerMonth') / 100;
            $numberOfStudyPerMonth = $numberOfStudy * 30;
            $increasedNumberOfStudyPerMonth = $numberOfStudyPerMonth + ($numberOfStudyPerMonth * $growthPerMonth);
            $convertedStudies = $this->getConvertedStudies($increasedNumberOfStudyPerMonth);
            $newTotalStudies = 0;

            foreach ($arrayOfMonths as &$month) {
                if($numberOfMonths > 1) {
                    if($newTotalStudies > 0) {
                        $increasedNumberOfStudyPerMonth = $newTotalStudies;
                    }
                    $newTotalStudies = $increasedNumberOfStudyPerMonth + ($numberOfStudyPerMonth * $growthPerMonth);
                    $convertedStudies = $this->getConvertedStudies($newTotalStudies);
                }
                $ramUsage = $this->getRamUsage($convertedStudies);

                $return[$month] = (object) [
                    'ram_usage'         => number_format($ramUsage, 2),
                    'number_of_studies' => number_format($increasedNumberOfStudyPerMonth, 2)
                ];
            }
        }
        
        return $return;
    }

    public function getRamUsage($convertedStudies) {
        $ramUsage = ($convertedStudies * $this->ramCost) * 24;
        return round($ramUsage, 2);
    }

    public function getConvertedStudies($numberOfStudyPerMonth) {
        $extraRam = $numberOfStudyPerMonth % 1000 > 0 ? 1 : 0;
        $intStudy = round($numberOfStudyPerMonth / 1000);
        return $extraRam + $intStudy;
    }
}