<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ForecastService;

class ForecastController extends Controller
{
    protected $forecastService;

	public function __construct(ForecastService $forecastService) {
		$this->forecastService = $forecastService;
	}

    public function getForecast(Request $request) {
    	return response()->json($this->forecastService->getForecast($request));
    }
}
