<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- favicon -->
        <link rel="icon" href="../assets/images/logo-mwci-mini.jpg">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/style/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- for date picker -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <title>Form</title>
        <link rel="stylesheet" href="../assets/style/page_style.css">
        <style>
            #form_list {
                font-size: 5px !important;
            }

            body { height: 1000px; }
            thead{
                background-color:white;
            }
        </style>
        <link rel="stylesheet" href="../assets/style/back_to_top.css">
    </head>
    <body>
        <center>
            <div class="container">
                <div class="modal-body">
                    <form id="employee_frm">

                        <div class="form-group">
                            <input type="text" name="study_per_day" id="study_per_day" class="form-control" maxlength="10" style="text-transform:uppercase" placeholder="Study per day">
                        </div>

                        <div class="form-group">
                            <input type="text" name="study_growth" id="study_growth" class="form-control" maxlength="50" style="text-transform:uppercase" placeholder="Study growth per month in %">
                        </div>

                        <div class="form-group">
                            <input type="text" name="number_of_months" id="number_of_months" class="form-control" maxlength="50" style="text-transform:uppercase" placeholder="Number of months to forecast">
                        </div>
                    <form>
                    <div class="modal-footer">
                        <input type="submit" id="submit_frm" class="btn btn-default" value="Submit">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Forecast</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row" id="table-container">
                                    <table class="table table-hover" id="form_list">
                                        <thead style="font-size: 12px !important;">
                                            <tr>
                                                <th>Month Year</th>
                                                <th>Number of Studies</th>
                                                <th>Cost Forecasted</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <table id="header-fixed"></table>
                                    <div id="no_data_div"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </center>

    </body>
    <script type="text/javascript">

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
                return false;
            }

            return true;
        }

        function ValidateFields(){
            if($("#study_per_day").val() === ""){
                alert("Please enter a value for study per day!");
                $("#study_per_day").focus();
                return false;
            }else if($("#study_growth").val() === ""){
                alert("Please enter a value for study growth!");
                $("#study_growth").focus();
                return false;
            }else if($("#number_of_months").val() === ""){
                alert("Please enter a value for study growth!");
                $("#number_of_months").focus();
                return false;
            }else{
                return true
            }
        }

        $("#submit_frm").click(function(e){
            if(!ValidateFields()){
                e.preventDefault();
            }else{
                e.preventDefault();
                $.ajax({
                    url: '/api/forecast',
                    type: 'GET',
                    data:{
                        'months'         : $("#number_of_months").val(),
                        'numberOfStudy'  : $("#study_per_day").val(),
                        'growthPerMonth' : $("#study_growth").val()

                    },
                    dataType: "json",
                    success: function(data) {
                        if(data){
                            Object.entries(data).forEach(([index, value]) => {
                                var html           = "";
                                html += '<tbody style="font-size: 10px !important;">';
                                var month          = index;
                                var numberOfStudy  = value.ram_usage;
                                var growthPerMonth = value.number_of_studies;

                                html += "<tr>";
                                    html += "<td>" + month + "</td>";
                                    html += "<td>" + numberOfStudy + "</td>";
                                    html += "<td>" + growthPerMonth + "</td>";
                                html += "<tr>";
                                html += "</tbody>";
                                $("#form_list").append(html);
                            });
                        }else{
                            $("#no_data_div").html("No Data Found!");
                        }
                    }
                });
            }
        });

    </script>
</html>